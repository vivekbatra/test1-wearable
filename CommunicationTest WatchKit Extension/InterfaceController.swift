//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {

    
    // MARK: Outlets
    // ---------------------
    
    var hunger : Int = 0
    var health : Int = 100
    var timerStatus = false
    
   
    @IBOutlet var feedButoonOutlet: WKInterfaceButton!
    
    @IBOutlet var startButtonOutlet: WKInterfaceButton!
    
    @IBOutlet var unHibernateButtonOutlet: WKInterfaceButton!
    
    
    @IBOutlet var hubernateButton: WKInterfaceButton!
    
    
    @IBOutlet var healthLabel: WKInterfaceLabel!
    @IBOutlet var hungerLabel: WKInterfaceLabel!
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    @IBOutlet var nameHim: WKInterfaceButton!
    
    var timer = Timer()
    var backgroundTimer = TimeInterval(0)
    
    @IBOutlet var appleTimer: WKInterfaceTimer!
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        let messageBody = message["course"] as! String
        
        if messageBody == "1"
        {
            self.pokemonImageView.setImageNamed("pikachu")
            self.nameHim.setHidden(false)
            

        }
        if messageBody == "2"
        {
            self.pokemonImageView.setImageNamed("caterpie")
            self.nameHim.setHidden(false)
            
        }
    }
    
    
//    private func session(_ session: WCSession, didReceiveMessage choice1: [String : Any]) {
//        print("Choice 1 received from phone")
//        // Message from phone comes in this format: ["course":"MADT"]
//        let choice = choice1["choice"] as! String
//        messageLabel.setText(choice)
//
//    }
    
    
    
//    private func session(_ session: WCSession, didReceiveMessage choice1: [String : Any]) {
//        print("Choice 1 received from phone")
//        // Message from phone comes in this format: ["course":"MADT"]
//        let messageBody = message["course"] as! String
//        messageLabel.setText(messageBody)
//    }
//    func session(_ session: WCSession, didReceiveMessage choice2: [String : Any]) {
//        print("Choice 2 received from phone")
//        // Message from phone comes in this format: ["course":"MADT"]
//        let messageBody = message["course"] as! String
//        messageLabel.setText(messageBody)
//    }
    


    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
        
//        if let name = context as? ANy
//        {
//            print(name)
//            print("naam aa gya")
//        }
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
         self.nameHim.setHidden(true)
        self.healthLabel.setHidden(true)
        self.hungerLabel.setHidden(true)
        self.nameLabel.setHidden(true)
        unHibernateButtonOutlet.setHidden(true)

        
        appleTimer.setDate(Date(timeIntervalSinceNow: 0))
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            WCSession.default.sendMessage(
                ["name" : "Pritesh"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
            })
        }
        else {
            print("Phone is not reachable")
        }
    }
    
    @objc func timerCountDown()
    {
        backgroundTimer += 1.0
        self.healthLabel.setHidden(false)
        self.hungerLabel.setHidden(false)
        self.nameLabel.setHidden(true)

        
        
        if(health == 0)
        {
            timer.invalidate()
            appleTimer.stop()
            self.nameLabel.setHidden(false)
            self.nameLabel.setText("This Pokemon is dead. Game Over")
            
            feedButoonOutlet.setHidden(true)
            startButtonOutlet.setHidden(true)
            hubernateButton.setHidden(true)
            unHibernateButtonOutlet.setHidden(true)
            
            
            

        }
        else{
            if (backgroundTimer.truncatingRemainder(dividingBy: 5) == 0)
            {
                self.hunger = hunger + 10
                
                
                if (self.hunger > 80)
                {
                    self.health = self.health - 5
                    
                }
            }
            
            self.hungerLabel.setText("Hunger : \(self.hunger)%")
            self.healthLabel.setText("Health : \(self.health)%")
            
        }
        
        
        
        
        
        
        
        
        
        
        
    }
    

    @IBAction func startButtonPressed()
    {
        
        let startTime = Date(timeIntervalSinceNow: 0)
        appleTimer.setDate(startTime)
        appleTimer.start()
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerCountDown), userInfo: nil, repeats: true)
        timerStatus = true
    }
    
    @IBAction func feedButtonPressed()
    {
        self.hunger = hunger - 12
    }
    
    @IBAction func hibernateButtonPressed() {
        timer.invalidate()
        appleTimer.stop()
        nameLabel.setText("Pokemon is hibernated")
        self.nameLabel.setHidden(false)
        unHibernateButtonOutlet.setHidden(false)
        hubernateButton.setHidden(true)

    }
    
    @IBAction func unHibernateButtonpressed()
    {
        appleTimer.start()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerCountDown), userInfo: nil, repeats: true)
        timerStatus = true
        unHibernateButtonOutlet.setHidden(true)
        hubernateButton.setHidden(false)
        
//        nameLabel.setText("Pokemon is hibernated")
//        self.nameLabel.setHidden(false)
    }
}

